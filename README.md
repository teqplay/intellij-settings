# IntelliJ settings

## Configuring the shared settings

Shared settings for IntelliJ are stored in this repository. Currently it only contains the Teqplay code style settings. To use this repository:

* Open your IntelliJ settings (Ctrl-Alt-S).
* Navigate to _Tools_ > _Settings Repository_.
* Under _Read-only sources_ click the '+'.
* Add `https://bitbucket.org/teqplay/intellij-settings.git/` as repository.
* Press the _Apply_ button.

Settings should be synced immediately, and when starting IntelliJ. To force a sync, open the `VCS` menu and use `Sync Settings` > `Merge`.

Note that you should __not__ use the option under _File_ > _Manage IDE Settings_ > _Settings Repository_, this option syncs _all_ your settings to/from the given repository, including the personal ones.

## Using the Teqplay code style

To use the shared code style:

* Open your IntelliJ settings (Ctrl-Alt-S).
* Navigate to _Editor_, _Code Style_.
* In the _Scheme_ dropdown in the top select the _Teqplay_ code style.

This will use the shared style for any new edits you will do. To reformat an entire file using the shared style, use _Code_ > _Reformat File..._. This is mainly useful when you're rewriting something almost completely, otherwise it will just introduce clutter in the commits.